﻿using Library.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.LogService.Domain.Models
{
    public class CustomException : Exception
    {
        private CustomId _id;

        public CustomException() { }
        public CustomException(Exception ex, CustomId id = null)
        {
            CustomMessage = ex.Message;
            CustomStackTrace = ex.StackTrace;
            CustomInnerMessage = ex.InnerException != null ? ex.InnerException.Message : String.Empty;
            CustomInnerStackTrace = ex.InnerException != null ? ex.InnerException.StackTrace : String.Empty;
            DateCreated = DateTime.Now;

            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }

        public string CustomMessage { get; set; }

        public string CustomStackTrace { get; set; }

        public string CustomInnerMessage { get; set; }

        public string CustomInnerStackTrace { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
