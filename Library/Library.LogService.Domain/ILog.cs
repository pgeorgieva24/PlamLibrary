﻿using Library.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.LogService.Domain
{
    public interface ILog
    {
        Task LogCustomExceptionAsync(Exception ex, CustomId id);
    }
}
