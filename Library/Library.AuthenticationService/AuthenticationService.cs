﻿using Library.UserService;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.AuthenticationService
{
    public class AuthService
    {
        public User LoggedUser { get; private set; }

        public async Task AuthenticateUser(string email)
        {
            UserManager manager = new UserManager();
            LoggedUser =  await manager.GetUserByEmailAsync(email);
        }
    }
}
