﻿using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Library.AuthenticationService
{
    public static class AuthenticationManager
    {
        public static User LoggedUser
        {
            get
            {
                AuthService authenticationService = null;

                if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                    HttpContext.Current.Session["LoggedUser"] = new AuthService();

                authenticationService = (AuthService)HttpContext.Current.Session["LoggedUser"];
                return authenticationService.LoggedUser;
            }
        }

        public static async Task Authenticate(string email)
        {
            AuthService authenticationService = null;

            if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                HttpContext.Current.Session["LoggedUser"] = new AuthService();

            authenticationService = (AuthService)HttpContext.Current.Session["LoggedUser"];

            await authenticationService.AuthenticateUser(email);
        }

        public static void Logout()
        {
            HttpContext.Current.Session["LoggedUser"] = null;
        }

        public static bool IsAdmin()
        {
            AuthService authenticationService = null;

            if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                HttpContext.Current.Session["LoggedUser"] = new AuthService();

            authenticationService = (AuthService)HttpContext.Current.Session["LoggedUser"];
            return authenticationService.LoggedUser.IsAdmin;
        }
    }
}
