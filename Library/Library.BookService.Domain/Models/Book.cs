﻿using Library.UserService.Domain.Models;
using Library.AuthorService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.GenreService.Domain.Models;
using Library.Common;
using System.ComponentModel.DataAnnotations;

namespace Library.BookService.Domain.Models
{
    public class Book
    {
        private CustomId _id;

        public Book() { }

        public Book(string title, string summary, string imgUrl, string authorId, CustomId id = null)
        {
            Title = title;
            Summary = summary;
            ImageUrl = imgUrl;
            AuthorId = authorId;
            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString() ?? new CustomId().ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }

        public string Title { get; set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public int TimesRead { get; set; }

        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }
        //public List<string> UsersMarkedAsRead { get; set; }
        //public virtual List<User> User { get; set; }

    }
}
