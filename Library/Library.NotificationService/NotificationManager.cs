﻿using Library.LogService;
using Library.LogService.Domain;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Library.NotificationService
{
    public class NotificationManager
    {
        private ILog _logger = Logger.GetInstance;
        private string confirmationEmailUrl = "http://plamlibrary.azurewebsites.net/Account/ValidateEmail";
        private string passwordResetUrl = "http://plamlibrary.azurewebsites.net/Account/PasswordReset";


        public async Task SendChangePasswordEmailAsync(User user)
        {
            string callbackUrl = $"{passwordResetUrl}?userId={user.Id}&validationCode={user.ValidationCode}";
            string link = $"<a href='{ callbackUrl}'>here</a>!";
            await SendEmailAsync(user.Email, "PlamLibrary change password request", $"Please reset your password by clicking {link}");
        }

        public async Task SendConfirmationEmailAsync(User user)
        {
            string callbackUrl = $"{confirmationEmailUrl}?userId={user.Id}&validationCode={user.ValidationCode}";
            string link = $"<a href='{ callbackUrl}'>here</a>!";
            await SendEmailAsync(user.Email, "PlamLibrary registration request", $"To confirm your account click  -> {link}");
        }

        private async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential("plamenag24@gmail.com", "shtegonapravim")
                };

                MailMessage mailMessage = new MailMessage
                {
                    From = new MailAddress("plamenag24@gmail.com")
                };
                mailMessage.To.Add(email);
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = subject;
                client.EnableSsl = true;
                await client.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            {
                await _logger.LogCustomExceptionAsync(ex, null);
                throw new ApplicationException($"Unable to load : '{ex.Message}'.");
            }
        }

    }
}
