﻿using Library.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.GenreService.Domain.Models
{
    public class Genre 
    {
        private CustomId _id;
        public Genre() { }
        public Genre(string name, CustomId id = null)
        {
            Name = name;
            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }
        public string Name { get; set; }

        
    }
}
