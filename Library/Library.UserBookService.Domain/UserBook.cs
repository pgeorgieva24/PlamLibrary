﻿using Library.BookService.Domain.Models;
using Library.Common;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.UserBookService.Domain
{
    public class UserBook
    {
        private CustomId _id;

        public UserBook() { }

        public UserBook(string bookId, string userId, CustomId id = null)
        {
            BookId = bookId;
            UserId = userId;
            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString() ?? new CustomId().ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }

        [ForeignKey("Book")]
        public string BookId { get; set; }
        public virtual Book Book { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
