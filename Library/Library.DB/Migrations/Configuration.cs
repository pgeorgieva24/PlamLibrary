namespace Library.DB.Migrations
{
    using Library.AuthorService.Domain.Models;
    using Library.BookGenresService.Domains;
    using Library.BookService.Domain.Models;
    using Library.Common;
    using Library.GenreService.Domain.Models;
    using Library.UserService.Domain.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {

            var users = new List<User>()
                {
                    new User("admin@admin.com", HashUtils.CreateHashCode("admin"),  HashUtils.CreateReferralCode(),true, true),
                    new User("pgeorgieva24@yahoo.com.com", HashUtils.CreateHashCode("plamena") ,HashUtils.CreateReferralCode(), false, true)
                };
            users.ForEach(s => context.Users.AddOrUpdate(p => p.Email, s));
            context.SaveChanges();

            var authors = new List<Author>()
                {
                    new Author( "Markus Zusak",  "https://images.gr-assets.com/authors/1376268260p5/11466.jpg" ),
                    new Author( "J.K. Rowling", "https://images.gr-assets.com/authors/1510435123p5/1077326.jpg" )
                };
            authors.ForEach(s => context.Authors.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var genres = new List<Genre>()
            {
                new Genre ( "Fantasy" ),
                new Genre ( "Fiction"),
                new Genre ( "Historical")
            };
            genres.ForEach(s => context.Genres.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var books = new List<Book>()
                {
                    new Book( "The Book Thief",
                        @"It�s just a small story really, about among other things: a girl, some words, an accordionist, 
                                    some fanatical Germans, a Jewish fist-fighter, and quite a lot of thievery ...",
                        "https://images.gr-assets.com/books/1390053681l/19063.jpg",
                        authors.Where(a=>a.Name== "Markus Zusak").FirstOrDefault().Id),

                    new Book(  "Fantastic Beasts and Where to Find Them",
                        @"When Magizoologist Newt Scamander arrives in New York, he intends his stay to be just a brief stopover. 
                                    However, when his magical case is misplaced and some of Newt's fantastic beasts escape, it spells trouble for everyone�",
                        "https://images.gr-assets.com/books/1481542648l/29363501.jpg",
                        authors.Where(a => a.Name == "J.K. Rowling").FirstOrDefault().Id ),

                    new Book( "Harry Potter and the Sorcerer's Stone",
                        @"Harry Potter's life is miserable. His parents are dead and he's stuck with his heartless relatives, who force him to live 
                                    in a tiny closet under the stairs. But his fortune changes when he receives a letter that tells him the truth about himself: 
                                    he's a wizard. A mysterious visitor rescues him from his relatives and takes him to his new home, Hogwarts School of Witchcraft 
                                    and Wizardry.",
                        "https://images.gr-assets.com/books/1474154022l/3.jpg",
                        authors.Where(a => a.Name == "J.K. Rowling").FirstOrDefault().Id)
                };
            books.ForEach(s => context.Books.AddOrUpdate(p => p.Title, s));
            context.SaveChanges();
            

            var bookGenres = new List<BookGenre>()
            {

                new BookGenre(books.Where(b=>b.Title=="The Book Thief").FirstOrDefault().Id, genres.Where(g=>g.Name== "Fiction").FirstOrDefault().Id),
                new BookGenre(books.Where(b=>b.Title=="The Book Thief").FirstOrDefault().Id, genres.Where(g=>g.Name== "Historical").FirstOrDefault().Id),

                new BookGenre(books.Where(b=>b.Title=="Fantastic Beasts and Where to Find Them").FirstOrDefault().Id, genres.Where(g=>g.Name== "Fantasy").FirstOrDefault().Id),
                new BookGenre(books.Where(b=>b.Title=="Fantastic Beasts and Where to Find Them").FirstOrDefault().Id, genres.Where(g=>g.Name== "Fiction").FirstOrDefault().Id),

                new BookGenre(books.Where(b=>b.Title=="Harry Potter and the Sorcerer's Stone").FirstOrDefault().Id, genres.Where(g=>g.Name== "Fantasy").FirstOrDefault().Id),
                new BookGenre(books.Where(b=>b.Title=="Harry Potter and the Sorcerer's Stone").FirstOrDefault().Id, genres.Where(g=>g.Name== "Fiction").FirstOrDefault().Id),

            };
            bookGenres.ForEach(s => context.BookGenres.AddOrUpdate(p => p.Id, s));
            context.SaveChanges();

        }
    }
}
