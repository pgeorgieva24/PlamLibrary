﻿using Library.AuthorService.Domain.Models;
using Library.BookGenresService.Domains;
using Library.BookService.Domain.Models;
using Library.GenreService.Domain.Models;
using Library.LogService.Domain.Models;
using Library.UserBookService.Domain;
using Library.UserService.Domain.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DB
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<BookGenre> BookGenres { get; set; }
        public DbSet<UserBook> UserBooks { get; set; }
        public DbSet<CustomException> CustomExceptions { get; set; }


        public ApplicationDbContext() : base(DbConnections.GetAzureConnection())
        {
            Users = Set<User>();
            Books = Set<Book>();
            Authors = Set<Author>();
            Genres = Set<Genre>();
            BookGenres = Set<BookGenre>();
            UserBooks = Set<UserBook>();
            CustomExceptions = Set<CustomException>();
        }
       
    }
}
