﻿using Library.Common;
using Library.DB;
using Library.LogService.Domain;
using Library.LogService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.LogService
{
    public sealed class Logger : ILog
    {
        private ApplicationDbContext _ctx = new ApplicationDbContext();

        private static readonly Lazy<Logger> instance =
            new Lazy<Logger>(() => new Logger());

        public static Logger GetInstance
        {
            get { return instance.Value; }
        }

        public async Task LogCustomExceptionAsync(Exception ex, CustomId id = null)
        {
            CustomException exception = new CustomException(ex, id);
            _ctx.CustomExceptions.Add(exception);
            await _ctx.SaveChangesAsync();
        }
    }
        
}
