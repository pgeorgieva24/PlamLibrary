﻿using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.NotificationService.Domain
{
    public interface INotificationActor
    {
        Task SendConfirmationEmailAsync(User user);
        Task SendChangePasswordEmailAsync(User user);
    }
}
