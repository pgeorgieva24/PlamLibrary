﻿using Library.Common;
using Library.DB;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.UserService
{
    public class UserManager
    {
        private readonly ApplicationDbContext _ctx;

        public UserManager()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<List<User>> GetAllUsersAsync() => await _ctx.Users.ToListAsync();

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await _ctx.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<User> GetUserByIdAsync(string id)
        {
            List<User> users = await _ctx.Users.ToListAsync();
            return users.FirstOrDefault(u => u.Id == id);
        }

        public async Task RegisterAsync(User user)
        {
            _ctx.Users.Add(user);
            await _ctx.SaveChangesAsync();
        }
        
    }
}
