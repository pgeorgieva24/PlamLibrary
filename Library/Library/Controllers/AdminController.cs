﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.AuthenticationService;
using Library.Common;
using Library.DB;
using Library.DTOs;
using Library.UserService.Domain.Models;
using System.Data.Entity.Migrations;
using Library.LogService.Domain.Models;
using Library.LogService.Domain;
using Library.LogService;

namespace Library.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationDbContext context;
        private ILog _logger = Logger.GetInstance;

        public AdminController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            try
            {
                if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
                {
                    List<User> users;
                    users = context.Users.ToList();
                    User admin = context.Users.SingleOrDefault(u => u.Id == AuthenticationManager.LoggedUser.Id);
                    users.Remove(admin);
                    return View(users);
                }
            }
            catch (Exception ex)
            {
                _logger.LogCustomExceptionAsync(ex, null);
            }

            return RedirectToAction("Error", "Home");
        }

        public ActionResult Edit(string id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = context.Users.SingleOrDefault(u => u.Id == id);
                if (user == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                UserEntry model = new UserEntry();
                model.Id = user.Id;
                model.IsAdmin = user.IsAdmin;

                return View(model);

            }

            return RedirectToAction("Error", "Home");
        }

        [HttpPost]
        public ActionResult Edit(UserEntry model)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = context.Users.SingleOrDefault(u => u.Id == model.Id);
                if (user == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                user = UpdateUserAdminRights(user, model.IsAdmin);
                context.Users.AddOrUpdate(user);
                context.SaveChanges();


                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Delete(string id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = context.Users.SingleOrDefault(u => u.Id == id);
                if (user == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                context.Users.Remove(user);
                context.SaveChanges();
                return RedirectToAction("Index");

            }
            return RedirectToAction("Error", "Home");
        }

        public ActionResult AllExceptions()
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                List<CustomException> exceptions = null;
                try
                {
                    exceptions = context.CustomExceptions
                        .OrderByDescending(e => e.DateCreated)
                        .Take(5)
                        .ToList();
                }
                catch (Exception ex)
                {
                    _logger.LogCustomExceptionAsync(ex, null);
                }
                return View(exceptions);
            }
            return RedirectToAction("Error", "Home");
        }

        private User UpdateUserAdminRights(User user, bool isAdmin)
        {
            var updatedUser = new User(
                user.Email,
                user.Password,
                user.ValidationCode,
                isAdmin,
                user.IsEmailConfirmed,
                new CustomId(new Guid(user.Id))
            );
            return updatedUser;
        }
    }
}
