﻿using Library.BookService.Domain.Models;
using Library.DB;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Library.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext context;
        

        public ActionResult Index()
        {
            context = new ApplicationDbContext();
            List<Book> books = context.Books.Include(b => b.Author).OrderByDescending(b => b.TimesRead).Take(3).ToList();
            return View(books);
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}