﻿using Library.AuthenticationService;
using Library.BookService.Domain.Models;
using Library.DB;
using Library.DTOs;
using Library.UserBookService.Domain;
using System.Data.Entity;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class UserBooksController : Controller
    {
        protected ApplicationDbContext context;
        public UserBooksController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index(string bookId = null)
        {
            if (AuthenticationManager.LoggedUser == null)
                return RedirectToAction("Login", "Home");

            List<UserBook> userBooks = context.UserBooks.Where(b => b.UserId == AuthenticationManager.LoggedUser.Id).ToList();
            UserEntry entry = new UserEntry(AuthenticationManager.LoggedUser.Id);

            if (bookId == null || context.Books.Where(b => b.Id == bookId).FirstOrDefault() == null)
            {
                SetBooksToUser(entry);
                return View(entry.BooksRead);
            }

            Book book = context.Books.Where(b => b.Id == bookId).FirstOrDefault();

            if (userBooks.Where(b => b.BookId == bookId).FirstOrDefault() == null)
            {
                UserBook userBook = new UserBook(bookId, AuthenticationManager.LoggedUser.Id);
                context.UserBooks.Add(userBook);
                book.TimesRead++;
                context.SaveChanges();
                SetBooksToUser(entry);
            }
            else
            {
                UserBook userBook = context.UserBooks.Where(b => b.BookId == bookId && b.UserId == AuthenticationManager.LoggedUser.Id).FirstOrDefault();
                context.UserBooks.Remove(userBook);
                book.TimesRead--;
                context.SaveChanges();
                SetBooksToUser(entry);
            }

            return View(entry.BooksRead);
        }

        private void SetBooksToUser(UserEntry entry)
        {
            List<UserBook> books = context.UserBooks.Where(b => b.UserId == AuthenticationManager.LoggedUser.Id).ToList();

            entry.BooksRead.Clear();
            foreach (var book in books)
            {
                entry.BooksRead.Add(context.Books.Include(b => b.Author).Where(b => b.Id == book.BookId).FirstOrDefault());
            }
        }
    }
}