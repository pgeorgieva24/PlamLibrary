﻿using Library.AuthenticationService;
using Library.Common;
using Library.DB;
using Library.DTOs;
using Library.LogService;
using Library.LogService.Domain;
using Library.NotificationService;
using Library.NotificationService.Domain;
using Library.UserService;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class AccountController : Controller
    {
        private UserManager _userManager = new UserManager();
        private ILog _logger = Logger.GetInstance;
        private NotificationManager _notificationManager = new NotificationManager();
        private ApplicationDbContext _ctx = new ApplicationDbContext();

        public ActionResult Welcome()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult ConfirmEmail()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginEntry entry)
        {
            ViewData["WrongLogin"] = null;
            if (entry.Email == null || entry.Password == null)
            {
                ViewData["WrongLogin"] = "Incorrect form!";
                return View();
            }

            try
            {
                var user = await _userManager.GetUserByEmailAsync(entry.Email);
                if (user != null)
                {
                    if (!user.IsEmailConfirmed)
                    {
                        ViewData["WrongLogin"] = "Email is not confirmed!";
                        return View(entry);
                    }
                    if (!HashUtils.VerifyPassword(entry.Password, user.Password))
                    {
                        ViewData["WrongLogin"] = "Incorrect Email or password!";
                        return View(entry);

                    }
                    await AuthenticationManager.Authenticate(user.Email);

                }
                if (user == null)
                {
                    ViewData["WrongLogin"] = "Incorrect Email or password!";
                    return View(entry);
                }

            }
            catch (Exception ex)
            {
                await _logger.LogCustomExceptionAsync(ex, null);
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterEntry entry)
        {
            try
            {
                User user = await _userManager.GetUserByEmailAsync(entry.Email);
                if (user != null)
                {
                    ViewData["WrongRegister"] = "Email already taken!";
                    return View(entry);
                }
                string password = HashUtils.CreateHashCode(entry.Password);
                string validationCode = HashUtils.CreateReferralCode();
                User newUser = new User(entry.Email, password, validationCode);

                await _userManager.RegisterAsync(newUser);
                await _notificationManager.SendConfirmationEmailAsync(newUser);
            }
            catch (Exception ex)
            {
                await _logger.LogCustomExceptionAsync(ex, null);
                return RedirectToAction("Error", "Home");
            }

            return View("Welcome");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ValidateEmail(string userId, string validationCode)
        {
            if (userId == null || validationCode == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            try
            {
                User user = await _userManager.GetUserByIdAsync(userId);
                if (user == null || validationCode != user.ValidationCode)
                {
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
                user = UpdateUserEmailConfirmation(user);
                _ctx.Entry(user).State = EntityState.Modified;
                await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                await _logger.LogCustomExceptionAsync(ex, null);
                return RedirectToAction("Error", "Home");
            }
            return View("ConfirmEmail");
        }


        public ActionResult Logout()
        {
            AuthenticationManager.Logout();
            return RedirectToAction("Index", "Home");
        }



        private User UpdateUserEmailConfirmation(User user)
        {
            var updatedUser = new User(
                user.Email,
                user.Password,
                user.ValidationCode,
                user.IsAdmin,
                true,
                new CustomId(new Guid(user.Id))
            );
            return updatedUser;
        }

    }
}