﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.AuthenticationService;
using Library.AuthorService.Domain.Models;
using Library.DB;
using Library.DTOs;
using System.Data.Entity.Migrations;

namespace Library.Controllers
{
    public class AuthorsController : Controller
    {
        private ApplicationDbContext context;
        public AuthorsController()
        {
            context = new ApplicationDbContext();
        }


        public ActionResult Index()
        {
            if (AuthenticationManager.LoggedUser == null || !AuthenticationManager.IsAdmin())
                return RedirectToAction("Error", "Home");
            return View(context.Authors.ToList());
        }

        public ActionResult Create()
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                AuthorEntry model = new AuthorEntry();
                return View(model);
            }
            return RedirectToAction("Login", "Account", null);

        }

        [HttpPost]
        public ActionResult Create(AuthorEntry model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                Author author = new Author(model.Name, model.ImageUrl);
                context.Authors.Add(author);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);
        }

        public ActionResult Edit(string id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Author author = context.Authors.SingleOrDefault(a => a.Id == id);

                if (author == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                AuthorEntry model = new AuthorEntry(author.Name, author.ImageUrl, author.Id);

                return View(model);
            }
            return RedirectToAction("Login", "Account", null);
        }
        [HttpPost]
        public ActionResult Edit(AuthorEntry model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Author author = context.Authors.SingleOrDefault(a => a.Id == model.Id);

                if (author == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                author.Name = model.Name;
                author.ImageUrl = model.ImageUrl;
                context.Entry(author).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);

        }

        public ActionResult Delete(string id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Author author = context.Authors.SingleOrDefault(a => a.Id == id);
                if (author == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                foreach (var item in context.Books)
                {
                    if (item.AuthorId == author.Id)
                    {
                        item.AuthorId = null;
                        context.Entry(item).State = EntityState.Modified;
                    }
                }
                context.Authors.Remove(author);
                context.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);

        }
    }
}
