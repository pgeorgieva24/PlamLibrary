﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.AuthenticationService;
using Library.BookGenresService.Domains;
using Library.BookService.Domain.Models;
using Library.DB;
using Library.DTOs;
using Library.UserBookService.Domain;

namespace Library.Controllers
{
    public class BooksController : Controller
    {
        private ApplicationDbContext context;

        public BooksController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            List<Book> books = context.Books.Include(b => b.Author).ToList();
            return View(SetGenresToBooks(books));
        }

        public ActionResult Create()
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                ViewBag.AuthorId = new SelectList(context.Authors, "Id", "Name");
                BookEntry entry = new BookEntry();
                entry.Genres = context.Genres.ToList();
                return View(entry);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(BookEntry entry)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                if (!ModelState.IsValid || Request["genre"] == null)
                {
                    ViewData["NoGenresSelected"] = null;

                    if (Request["genre"] == null)
                    {
                        ViewData["NoGenresSelected"] = "You haven't selected a genre!";
                    }
                    ViewBag.AuthorId = new SelectList(context.Authors, "Id", "Name");
                    if (Request["genre"] != null)
                        entry.GenresIds = GetGenresFromCb();
                    entry.Genres = context.Genres.ToList();
                    return View(entry);
                }
                Book book = new Book(entry.Title, entry.Summary, entry.ImageUrl, entry.AuthorId);
                context.Books.Add(book);
                context.SaveChanges();
                SetGenresToBook(book.Id);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {

                Book book = context.Books.Include(b => b.Author).FirstOrDefault(b => b.Id == id);
                if (book == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                BookEntry entry = new BookEntry(book.Id, book.Title, book.Summary, book.ImageUrl, book.TimesRead, book.AuthorId, book.Author);
                ViewBag.AuthorId = new SelectList(context.Authors, "Id", "Name", entry.AuthorId);
                foreach (var bookGenre in context.BookGenres.Where(b => b.BookId == id))
                {
                    entry.GenresIds.Add(bookGenre.GenreId);
                }
                entry.Genres = context.Genres.ToList();

                return View(entry);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(BookEntry entry)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid || Request["genre"] == null)
                {
                    ViewData["NoGenresSelected"] = null;

                    if (Request["genre"] == null)
                    {
                        ViewData["NoGenresSelected"] = "You haven't selected a genre!";
                    }
                    ViewBag.AuthorId = new SelectList(context.Authors, "Id", "Name", entry.AuthorId);
                    if (Request["genre"] != null)
                        entry.GenresIds = GetGenresFromCb();
                    entry.Genres = context.Genres.ToList();
                    return View(entry);
                }

                Book book = context.Books.Find(entry.Id);

                if (book == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                book.AuthorId = entry.AuthorId;
                book.Title = entry.Title;
                book.Summary = entry.Summary;
                book.ImageUrl = entry.ImageUrl;
                context.Entry(book).State = EntityState.Modified;

                if (GetGenresFromCb() != entry.GenresIds)
                {
                    foreach (var item in context.BookGenres)
                    {
                        if (item.BookId == entry.Id)
                        {
                            context.BookGenres.Remove(item);
                        }
                    }
                    SetGenresToBook(book.Id);
                }
                context.SaveChanges();

                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Book book = context.Books.Find(id);
            if (book == null)
            {
                return RedirectToAction("Error", "Home");
            }
            foreach (var item in context.BookGenres)
            {
                if (item.BookId == book.Id)
                {
                    context.BookGenres.Remove(item);
                }
            }
            foreach (var item in context.UserBooks)
            {
                if (item.BookId == book.Id)
                {
                    context.UserBooks.Remove(item);
                }
            }
            context.Books.Remove(book);
            context.SaveChanges();
            return RedirectToAction("Index");
        }


        #region manage book genres

        private List<BookEntry> SetGenresToBooks(List<Book> books)
        {


            List<BookEntry> entries = new List<BookEntry>();

            foreach (var book in books)
            {
                BookEntry entry = new BookEntry(book.Id, book.Title, book.Summary, book.ImageUrl, book.TimesRead, book.AuthorId, book.Author);
                if (context.BookGenres.Any(b => b.BookId == book.Id))
                {
                    foreach (var genre in context.BookGenres.Where(b => b.BookId == book.Id).ToList())
                    {
                        entry.Genres.Add(genre.Genre);
                    }
                }
                if (AuthenticationManager.LoggedUser != null && context.UserBooks.Where(b => b.UserId == AuthenticationManager.LoggedUser.Id && b.BookId == book.Id).FirstOrDefault() != null)
                    entry.IsRead = true;

                entries.Add(entry);
            }
            return entries;

        }

        private List<string> GetGenresFromCb()
        {
            string cbGenres = Request["genre"];
            int starIndex = 0;
            List<string> genres = new List<string>();
            while (cbGenres != null)
            {
                string id;
                id = cbGenres.Substring(starIndex, 36);
                genres.Add(id);
                if (cbGenres.Length != 36)
                {
                    cbGenres = cbGenres.Substring(37, cbGenres.Length - 37);
                }
                else cbGenres = null;
            }
            return genres;
        }
        private void SetGenresToBook(string bookId)
        {
            List<string> cbGenres = GetGenresFromCb();
            BookGenre bookGenre;
            foreach (var item in cbGenres)
            {
                bookGenre = new BookGenre(bookId, item);
                context.BookGenres.Add(bookGenre);
                context.SaveChanges();
            }
        }


        #endregion
    }
}
