﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.AuthenticationService;
using Library.DB;
using Library.DTOs;
using Library.GenreService.Domain.Models;

namespace Library.Controllers
{
    public class GenresController : Controller
    {
        private ApplicationDbContext context;
        public GenresController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            if (AuthenticationManager.LoggedUser == null || !AuthenticationManager.IsAdmin())
                return RedirectToAction("Error", "Home");

            return View(context.Genres.ToList());
        }

        public ActionResult Create()
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                GenreEntry model = new GenreEntry();
                return View(model);
            }
            return RedirectToAction("Login", "Account", null);
        }

        [HttpPost]
        public ActionResult Create(GenreEntry model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                Genre genre = new Genre(model.Name);
                context.Genres.Add(genre);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);
        }

        public ActionResult Edit(string id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Genre genre = context.Genres.SingleOrDefault(a => a.Id == id);

                if (genre == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                GenreEntry model = new GenreEntry(genre.Name, genre.Id);

                return View(model);
            }
            return RedirectToAction("Login", "Account", null);
        }


        [HttpPost]
        public ActionResult Edit(GenreEntry model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Genre genre = context.Genres.SingleOrDefault(a => a.Id == model.Id);

                if (genre == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                genre.Name = model.Name;

                context.Entry(genre).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);
        }

        public ActionResult Delete(string id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Genre genre = context.Genres.SingleOrDefault(a => a.Id == id);
                if (genre == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                foreach (var item in context.BookGenres)
                {
                    if (item.GenreId == genre.Id)
                    {
                        context.BookGenres.Remove(item);
                    }
                }
                context.Genres.Remove(genre);
                context.SaveChanges();


                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Account", null);
        }

    }
}
