﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.DTOs
{
    public class AuthorEntry
    {
        public string Id { get; set; }
        [Required]
        [RegularExpression("[A-Za-z.' ]+", ErrorMessage = "Name should contain letters only")]
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        public AuthorEntry()
        {

        }
        public AuthorEntry(string name, string imgUrl, string id )
        {
            Name = name;
            ImageUrl = imgUrl;
            Id = id;
        }
    }
}