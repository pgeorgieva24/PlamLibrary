﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.DTOs
{
    public class GenreEntry
    {
        public GenreEntry() { }
        public GenreEntry(string name, string id)
        {
            Name = name;
            Id = id;
        }

        public string Id { get; set; }
        [Required]
        [RegularExpression("[A-Za-z_ ]+", ErrorMessage = "Genre should contain letters only")]
        public string Name { get; set; }
    }
}