﻿using Library.BookService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.DTOs
{
    public class UserEntry
    {
        public UserEntry() { } 
        public UserEntry(string id)
        {
            Id = id;
            BooksRead = new List<Book>();
        }
        public string Id { get; set; }
        public bool IsAdmin { get; set; }

        public List<Book> BooksRead { get; set; }
    }
}