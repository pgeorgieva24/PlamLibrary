﻿using Library.AuthorService.Domain.Models;
using Library.BookGenresService.Domains;
using Library.GenreService.Domain.Models;
using Library.UserService.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.DTOs
{
    public class BookEntry
    {
        public BookEntry()
        {
            Genres = new List<Genre>();
            GenresIds = new List<string>();
        }
        public BookEntry(string id, string title, string summary, string imageUrl, int timesRead, string authorId, Author author)
        {
            Id = id;
            Title = title;
            Summary = summary;
            ImageUrl = imageUrl;
            TimesRead = timesRead;
            AuthorId = authorId;
            Author = author;
            Genres = new List<Genre>();
            GenresIds = new List<string>();
        }

        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public int TimesRead { get; set; }

        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }
        public bool IsRead{ get; set; }
        public List<string> GenresIds { get; set; }
        public List<Genre> Genres { get; set; }

    }
}