﻿using Library.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.AuthorService.Domain.Models
{
    public class Author 
    {
        private CustomId _id;
        public Author() { }
        public Author(string name, string imgUrl, CustomId id = null)
        {
            Name = name;
            ImageUrl = imgUrl;
            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        
    }
}
