﻿using Library.BookService.Domain.Models;
using Library.Common;
using Library.GenreService.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BookGenresService.Domains
{
    public class BookGenre
    {
        private CustomId _id;


        public BookGenre()
        {

        }

        public BookGenre(string bookId, string genreId, CustomId id=null)
        {
            BookId = bookId;
            GenreId = genreId;
            _id = id ?? new CustomId();
        }

        [Key]
        public string Id
        {
            get { return _id.ToString() ?? new CustomId().ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }
        
        [ForeignKey("Book")]
        public string BookId { get; set; }
        public virtual Book Book { get; set; }

        [ForeignKey("Genre")]
        public string GenreId { get; set; }
        public virtual Genre Genre { get; set; }
    }
}
