﻿using Library.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Library.UserService.Domain.Models
{
    public class User
    {
        private CustomId _id;

        public User() { }

       
        public User( string email, string password, string validationCode = null, bool isAdmin = false, bool isEmailConfirmed = false, CustomId id = null )
        {
            Email = email;
            Password = password;
            IsAdmin = isAdmin;
            ValidationCode = validationCode;
            IsEmailConfirmed = isEmailConfirmed;
            _id = id ?? new CustomId();

        }

        [Key]
        public string Id
        {
            get { return _id.ToString(); }
            private set { _id = new CustomId(new Guid(value)); }
        }
        

        public string Email { get; private set; }

        public string Password { get; private set; }

        public bool IsAdmin { get; private set; }

        public string ValidationCode { get; private set; }

        public bool IsEmailConfirmed { get; private set; }

        //public List<string> BooksReadIds { get; set; }

    }
}
